from netmiko import ConnectHandler
import time

# paramètres de connexion pour netmiko : vides au début
device = {
'device_type': 'cisco_ios_telnet',
'ip': '','password': '','secret':''
}

####remplir les paramètres de connexion##########
device['password']=input("Entrer le mot de passe telnet: ")
device['secret']=input("Entrer le secrer (pour passer au mode prévilégié): ")

########préparer la liste de commandes à exécuter########
configfile=open("configfile.txt")
configset=configfile.read().split("\n") # la liste est prête...
configfile.close()

####exécuter les commandes sur tous les équipements#######
fichier_ip=open("ip.txt")
for line in fichier_ip:
    device['ip']=line.strip("\n")  # lire l'adresse IP de l'équipement
    print("\n\nConnexion a l'equipement Cisco ",device['ip'])
    net_connect = ConnectHandler(**device)   #se connecter avec la config de la variable device
    net_connect.enable()
    time.sleep(2)
    print ("Envoi des commandes vers votre equipement...")
    net_connect.send_config_set(configset)       #exécuter la config
    print ("Configuration de l'equipement ",device['ip'], " terminee")
    net_connect.disconnect()     #déconnecter de cet équipement
fichier_ip.close()
